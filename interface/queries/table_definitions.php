<?php

require_once("../globals.php");
require_once(dirname(__FILE__) . "/../../library/queries/sql.inc");
require_once(dirname(__FILE__) . "/functions.inc");

header('Content-Type: application/json');
echo json_encode(get_category_list($_GET["table"]));
?>
