<?php
/**
 * Query results screen.
 *
 * @package OpenEMR
 * @link http://www.open-emr.org
 * @license https://github.com/openemr/openemr/blob/master/LICENSE GNU General Public License 3
 */

require_once("../globals.php");
require_once("$srcdir/options.inc.php");
require_once(dirname(__FILE__) . "/../../vendor/php-paginator-master/src/JasonGrimes/Paginator.php");
require_once(dirname(__FILE__) . "/../../library/queries/sql.inc");
require_once(dirname(__FILE__) . "/functions.inc");
//require_once("table_definitions.inc");

use JasonGrimes\Paginator;

$MAXSHOW = 100; // maximum number of results to display at once

$popup  = empty($_REQUEST['popup']) ? 0 : 1;
$message = isset($_GET['message']) ? $_GET['message'] : "";
$from_page = isset($_REQUEST['from_page']) ? $_REQUEST['from_page'] : "";

$data = $_GET;
$tables = $data['table'];
$categories = $data['category'];
$operands = &$data['operand'];
$values = &$data['value'];

$current_page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$orderby = 'person.lname, person.fname';
$sqllimit = $MAXSHOW;
$count = 0;

$fstart = ($current_page-1) * $sqllimit;

unset($data["fstart"]);
unset($data["page"]);
?>


<!DOCTYPE html>
<html>
<head>
<?php html_header_show();?>
<script type="text/javascript" src="<?php echo $webroot ?>/interface/main/tabs/js/include_opener.js"></script>

<link href="<?php echo $GLOBALS['assets_static_relative']; ?>/bootstrap-3-3-4/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
form {
    padding: 0px;
    margin: 0px;
}
#searchCriteria {
    text-align: center;
    width: 100%;
    font-size: 0.8em;
    background-color: #ddddff;
    font-weight: bold;
    padding: 3px;
}
#searchResults {
    width: 96%;
    border-collapse: collapse;
}
#searchResults thead {
    background-color: lightgrey;
}
#searchResults th,
#searchResults td {
    font-size: 0.7em;
    padding: 2px;
}
#searchResults tbody {
    height: 80%;
    overflow: auto;
}

table#searchResults {
    width: 100%;
    border-collapse: collapse;
    background-color: white;
}
#searchResults td {
    border-bottom: 1px solid #eee;
}
.billing { color: red; font-weight: bold; }
.highlight {
    background-color: #336699;
    color: white;
}

tr.item:hover {
  cursor: pointer;
}

/* pace loading bar */
.pace {
  -webkit-pointer-events: none;
  pointer-events: none;

  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;

  z-index: 2000;
  position: fixed;
  margin: auto;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  height: 5px;
  width: 200px;
  background: #fff;
  border: 1px solid #29d;

  overflow: hidden;
}

.pace .pace-progress {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  -ms-box-sizing: border-box;
  -o-box-sizing: border-box;
  box-sizing: border-box;

  -webkit-transform: translate3d(0, 0, 0);
  -moz-transform: translate3d(0, 0, 0);
  -ms-transform: translate3d(0, 0, 0);
  -o-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0);

  max-width: 200px;
  position: fixed;
  z-index: 2000;
  display: block;
  position: absolute;
  top: 0;
  right: 100%;
  height: 100%;
  width: 100%;
  background: #29d;
}

.pace.pace-inactive {
  display: none;
}

</style>

<script type="text/javascript" src="<?php echo $GLOBALS['assets_static_relative']; ?>/jquery-min-3-1-1/index.js"></script>
<script type="text/javascript" src="<?php echo $webroot ?>/library/js/pace.min.js"></script>
<script type="text/javascript" src="<?php echo $webroot ?>/library/js/js-url-2.5.3/url.min.js"></script>
<script type="text/javascript" src="<?php echo $webroot ?>/node_modules/mustache/mustache.min.js"></script>

<?php if ($popup) { ?>
<script type="text/javascript" src="../../library/topdialog.js"></script>
<?php } ?>

<script language="JavaScript">
<?php if ($popup) {
    require($GLOBALS['srcdir'] . "/restoreSession.php");
} ?>
// This is called when forward or backward paging is done.
//
function submitList(offset) {
 var f = document.forms[0];
 var i = parseInt(f.fstart.value) + offset;
 if (i < 0) i = 0;
 f.fstart.value = i;
 top.restoreSession();
 f.submit();
}

function export_csv() {
  let url = './export_csv.php' + window.location.search;
  window.location.href = url;
}
</script>

<script language="javascript">

// jQuery stuff to make the page a little easier to use

$(document).ready(function() {
  $.get({
    url: 'exec_query.php' + window.location.search,
    success: function(result) {
      console.log(result);
      var template = $('#template').html();
      var rendered = Mustache.render(template, result);
      rendered = rendered.replace(/\.br/g, '<br>');
      rendered = rendered.replace(/\|\|/g, '<br>');
      $('#search-results').html(rendered);

      $(".item").mouseover(function() { $(this).addClass("highlight"); });
      $(".item").mouseout(function() { $(this).removeClass("highlight"); });
      $(".item").click(function() {
        window.open($(this).data("href"), '_blank');
      });
      $("#export").click(function() {
        export_csv();
      });
    },
    timeout: 60000,
    error: function(request, statusText, error) {
      let message = 'An error has occurred. Please try again.';
      if (statusText == 'timeout') {
        message = 'Request timed out. Please try again.';
      } else if (error) {
        message = error;
      }
      $('#search-results').html('<span>' + message + '</span>');
    }
  });
});

</script>

</head>
<body class="body_top">

<?php if ($message) {
    echo "<font color='red'><b>".htmlspecialchars($message, ENT_NOQUOTES)."</b></font>\n";
} ?>
<div id="search-results"></div>

<script id="template" type="x-tmpl-mustache">
  <table border='0' cellpadding='5' cellspacing='0' width='100%' style="">
   <tr>
    <td class='text' align='center'>
    {{{paginator}}}
    </td>
   </tr>
   <tr>
    <td class='text' align='right'>
    {{count_statement}}
    </td>
   </tr>
  </table>

  <table id="searchResults">
    <thead>
      <tr>
        {{#columns}}
        <th>{{{.}}}</th>
        {{/columns}}
      </tr>
    </thead>
    <tbody>
      {{#data}}
      <tr class='item' data-href="{{{link}}}">
        {{#values}}
        <td>{{.}}</td>
        {{/values}}
      </tr>
      {{/data}}
      {{^data}}
      <tr class='empty'>
        <td>No results</td>
      </tr>
      {{/data}}
    </tbody>
  </table> <!-- end searchResults TABLE -->

  <table border='0' cellpadding='5' cellspacing='0' width='100%' style="">
    <tr>
      <td class='text' align='center'>
        {{{paginator}}}
      </td>
    </tr>
    <tr>
      <td style="display:flex;">
        <button type="button" class="btn btn-default btn-download" id="export" value="<?php echo xla('Export as CSV'); ?>" style="margin:auto;">
            <?php echo xlt('Export as CSV'); ?>
        </button>
      </td>
    </tr>
  </table>
</script>

<script language="javascript">

// jQuery stuff to make the page a little easier to use

$(document).ready(function(){
    $(".oneresult").mouseover(function() { $(this).addClass("highlight"); });
    $(".oneresult").mouseout(function() { $(this).removeClass("highlight"); });
    $("#export").click(function() {
      export_csv();
    });
});

</script>

</body>
</html>
