<?php

require_once("../globals.php");
require_once(dirname(__FILE__) . "/../../library/queries/sql.inc");

const ARRAY_PERSON = array(
  'id'=>'Adept Id',
  'title'=>'Title',
  'pname'=>'Pname',
  'fname'=>'First Name',
  'mname'=>'Mname',
  'lname'=>'Last Name',
  'maidenname'=>'Maidenname',
  'aka'=>'Aka',
  'phn'=>'Phn',
  'other_ins'=>'Other Ins',
  'other_ins_type'=>'Other Ins Type',
  'bday'=>'Birthday',
  'sex'=>'Gender',
  'marital'=>'Marital',
  'ethnicity'=>'Ethnicity',
  'ethnicity_other'=>'Other Ethnicity ',
  'birthplace'=>'Birth Place',
  'datemoved'=>'Moved to Canada',
  'street1'=>'Street 1',
  'street2'=>'Street 2',
  'city'=>'City',
  'pcode'=>'Post Code',
  'province'=>'Province',
  'country'=>'Country',
  'bil_sameas'=>'Bill Sameas',
  'bil_street1'=>'Bill Street1',
  'bil_street2'=>'Bill Street2',
  'bil_city'=>'Bill City',
  'bil_pcode'=>'Bill Postcode',
  'bil_province'=>'Bill Province',
  'bil_country'=>'Bill Country',
  'bil_notes'=>'Bill Notes',
  'htel'=>'Home Phone',
  'mtel'=>'Mobility Phone',
  'wtel'=>'Work Phone',
  'otel'=>'Other Phone',
  'pager'=>'Pager',
  'fax'=>'Fax',
  'voicemail'=>'Voicemail',
  'email'=>'Email',
  'emcont'=>'Emcont',
  'prof_number'=>'Prof Number',
  'payee_number'=>'Payee Number',
  'is_nurse_pract'=>'Is Nurse Pract',
  'prof_status'=>'Prof Status',
  'prof_description'=>'Prof Description',
  'prof_description_special'=>'Prof Description Special',
  'prof_description_sec'=>'Prof Description Sec',
  'prof_description_sec_special'=>'Prof Description Sec Special',
  'prof_description_third'=>'Prof Description Third',
  'prof_description_third_special'=>'Prof Description Third Special',
  'prof_description_forth'=>'Prof Description Forth',
  'prof_description_forth_special'=>'Prof Description Forth Special',
  'prof_description_fifth'=>'Prof Description Fifth',
  'prof_description_fifth_special'=>'Prof Description Fifth Special',
  'signature'=>'Signature',
  'type'=>'Type',
  'picture'=>'Picture',
  'notes'=>'Notes',
  'notes_consultant'=>'Notes Consultant',
  'notes_booking'=>'Notes Booking',
  'notes_arrival'=>'Notes Arrival',
  'notes_billing'=>'Notes Billing',
  'notes_invoice'=>'Notes Invoice',
  'deceased'=>'Deceased',
  'deceased_location'=>'Deceased Location',
  'deceased_cause'=>'Deceased Cause',
  'deceased_autopsy'=>'Deceased Autopsy',
  'deceased_autopsy_results'=>'Deceased Autopsy Results',
  'last_visit_date'=>'Last_visit_date',
  'ins_last_elig'=>'Ins last Elig',
  'external_id'=>'External_id',
  'imported'=>'Imported'
);


const ARRAY_WK_INCOHRS = array(
  'adept_id'=>'Adept Id',
  'released'=>'Released',
  'lname'=>'Last Name',
  'fname'=>'First Name',
  'tx'=>'Tx',
  'gender'=>'Gender',
  'bday'=>'Date of Birth',
  'date_of_diagnosis'=>'Date Of Diagnosis',
  'hiv'=>'HIV',
  'ethnicity'=>'Ethnicity',
  're_infection'=>'Re Infection',
  'methadone'=>'Methadone',
  'blood_transfusion'=>'Blood Transfusion',
  'tattoos'=>'Tattoos',
  'drug_use'=>'Drug Use',
  'where_ivdu'=>'Where Ivdu',
  'alcohol'=>'Alcohol',
  'cigarettes'=>'Cigarettes',
  'mental_health'=>'Mental Health',
  'suicide'=>'Suicide',
  'education'=>'Education',
  'reason_test'=>'Reason Test',
  'prev_tx'=>'Prev Tx',
  'tx_start_vs_dx_date'=>'Tx Start Date Vs Dx Date',
  'tx_delay_reason'=>'The Reason For Delaying Tx',
  'genotype'=>'Genotype',
  'duration'=>'Duration',
  'tx_start_date'=>'Tx Start Date',
  'tx_end_date'=>'Tx End Date',
  'fibrosis_stage'=>'Fibrosis Stage',
  'pre_alt'=>'Pre Alt',
  'pre_ast'=>'Pre Ast',
  'end_alt'=>'End Alt',
  'end_ast'=>'End Ast',
  'week_0'=>'Week 0',
  'week_4'=>'Week 4',
  'week_12'=>'Week 12',
  'week_20'=>'Week 20',
  'week_24'=>'Week 24',
  'week_28'=>'Week 28',
  'week_36'=>'Week 36',
  'week_40'=>'Week 40',
  'week_44'=>'Week 44',
  'week_48'=>'Week 48',
  'week_60'=>'Week 60',
  'week_72'=>'Week 72',
  'week_120'=>'Week 120',
  'comments'=>'Comments',
  'rvr'=>'Rvr',
  'evr'=>'Evr',
  'eot'=>'Eot',
  'svr'=>'Svr',
  'geno_1_svr'=>'Geno 1 Svr',
  'geno_2_eot'=>'Geno 2 Eot',
  'geno_2_svr'=>'Geno 2 Svr',
  'geno_3_eot'=>'Geno 3 Eot',
  'geno_3_svr'=>'Geno 3 Svr',
  'geno_4'=>'Geno 4',
  'ivdu'=>'Ivdu',
  'svr_and_idu'=>'Svr And Idu',
  'did_not_finish'=>'Did Not Finish',
  'pegasys'=>'Pegasys',
  'finished'=>'Finished',
  'finished_and_did_not_do_svr'=>'Finished And Did Not Do Svr',
  'relapsed'=>'Relapsed',
  'non_complyent'=>'Non Complyent',
  'not_finish_due_to_side_effect'=>'Not Finish Due To Side Effect',
  'not_finish'=>'Not Finish',
  'loss_of_follow_up'=>'Loss Of Follow Up',
  'did_not_finish_1'=>'Did Not Finish 1',
  'no_respond'=>'No Respond',
  'side_effects'=>'Side Effects',
  'treatment_outcome'=>'Treatment Outcome',
  'svr_2'=>'Svr 2',
  'death'=>'Death',
  'pretxcd4'=>'Pretxcd4',
  'pretxhiv_load'=>'Pretxhiv Load',
  'on_haart'=>'On Haart',
  'idub4tx'=>'Idub4tx',
  'idu_during_tx'=>'Idu During Tx',
  'idu_after_tx'=>'Idu After Tx',
  'methadone_2'=>'Methadone 2',
  'meth_b4'=>'Meth B4',
  'meth_during'=>'Meth During',
  'meth_post_tx'=>'Meth Post Tx',
  'tattoo'=>'Tattoo',
  'idu'=>'Idu',
  'confirm'=>'Confirm',
  'confirmedby'=>'Confirmed By',
  'timestamp'=>'Timestamp'
);

const ARRAY_WK_GENOBIOPSY = array(
  'adept_id'=>'Adept Id',
  'lname'=>'Last Name',
  'fname'=>'First Name',
  'ptype'=>'Patient Type',
  'hepatitis'=>'Hepatitis',
  'genotype'=>'Genotype',
  'tx_number'=>'Tx Number',
  'tx_start_date'=>'Tx Actual Start Date',
  'tx_eduring'=>'Tx Schedule Duration',
  'tx_estop_date'=>'Tx Schedule Stop Date',
  'txstart_date'=>'Tx Actual Start Date',
  'tx_astop_date'=>'Actual Stop Date ',
  'tx_aduring'=>'Tx Actual Duration',
  'tx_type'=>'Tx Type',
  'tx_type1'=>'Tx Type1',
  'din'=>'Din',
  'dosage'=>'Dosage',
  'ribavirin'=>'Ribavirin',
  'ribavirin2'=>'Ribavirin 1',
  'de99th'=>'De99th',
  'deceased'=>'Deceased',
  'deceased_cause'=>'Deceased Cause',
  'per_blood'=>'Per Blood',
  'per_illicitdrug'=>'Per Illicit Drug',
  'per_injdrug'=>'Per Injdrug',
  'per_methadone'=>'Per Methadone',
  'per_tattoo'=>'Per Tattoo',
  'per_weight'=>'Per Weight',
  'year_likely_infected'=>'Year Likely Infected',
  'year_hbv'=>'Year Hbv Diagnosed',
  'year_hcv'=>'Year Hcv Diagnosed',
  'w4txout'=>'Week 4 Tx Outcome',
  'w12txout'=>'Week 12 Tx Outcome',
  'eotout'=>'End Of Tx Outcome',
  'txoutcome'=>'Tx Outcome',
  'late_relapser'=>'Late Relapser',
  'txperiod'=>'Tx Period',
  'doctor_decision'=>'Doctor Decision',
  'genotype_original'=>'Original Genotype',
  'genotype_reinfected'=>'Reinfected Genotype',
  'fibor'=>'Fiborsis',
  'ultrasound'=>'Ultrasound',
  'reinfection'=>'Reinfection',
  'hiv'=>'Hiv',
  'time_to_ri'=>'Time To Reinfacted',
  'censor'=>'Censor',
  'initiated_location'=>'Where Initiated Drug Use',
  'initiated_drug'=>'Drug Choice',
  'initiated_inprison'=>'Used In Prison',
  'current_drugs_used'=>'Current Drugs Used',
  'idu_start_year'=>'Idu Start Year',
  'idub4tx'=>'Idu Befor Tx',
  'idu_during_tx'=>'Idu During Tx',
  'idu_after_tx'=>'Idu After Tx',
  'methadone'=>'Methadone',
  'meth_b4'=>'Meth Befor Tx',
  'meth_during'=>'Meth During',
  'meth_post_tx'=>'Meth Post Tx',
  'tattoo'=>'Tattoo',
  'idu'=>'Idu',
  'blood_splash_after_tx'=>'Blood Splash After Tx',
  'bx_date'=>'Bx Date',
  'fiborsis_stage'=>'Fiborsis Stage',
  'bx_detail'=>'Biopsy Detail',
  'bx_panel'=>'Biopsy Specimen #',
  'bx_dr'=>'Biopsy Pathologist',
  'tx_notes'=>'Tx Notes',
  'alt1'=>'Alt1',
  'alt2'=>'Alt2',
  'pharma'=>'Pharma',
  'cashflow_w1'=>'Cashflow W1',
  'cashflow_w2'=>'Cashflow W2',
  'cashflow_w3'=>'Cashflow W3',
  'cashflow_w4'=>'Cashflow W4',
  'cashflow_w5'=>'Cashflow W5',
  'cashflow_w6'=>'Cashflow W6',
  'cashflow_w7'=>'Cashflow W7',
  'cashflow_w8'=>'Cashflow W8',
  'cashflow_w9'=>'Cashflow W9',
  'cashflow_w10'=>'Cashflow W10',
  'cashflow_w11'=>'Cashflow W11',
  'cashflow_w12'=>'Cashflow W12',
  'cashflow_w13'=>'Cashflow W13',
  'cashflow_w14'=>'Cashflow W14',
  'cashflow_w15'=>'Cashflow W15',
  'cashflow_w16'=>'Cashflow W16',
  'cashflow_w17'=>'Cashflow W17',
  'cashflow_w18'=>'Cashflow W18',
  'cashflow_w19'=>'Cashflow W19',
  'cashflow_w20'=>'Cashflow W20',
  'cashflow_w21'=>'Cashflow W21',
  'cashflow_w22'=>'Cashflow W22',
  'cashflow_w23'=>'Cashflow W23',
  'cashflow_w24'=>'Cashflow W24',
  'cashflow_w25'=>'Cashflow W25',
  'cashflow_w26'=>'Cashflow W26',
  'cashflow_w27'=>'Cashflow W27',
  'cashflow_w28'=>'Cashflow W28',
  'cashflow_w29'=>'Cashflow W29',
  'cashflow_w30'=>'Cashflow W30',
  'cashflow_w31'=>'Cashflow W31',
  'cashflow_w32'=>'Cashflow W32',
  'cashflow_w33'=>'Cashflow W33',
  'cashflow_w34'=>'Cashflow W34',
  'cashflow_w35'=>'Cashflow W35',
  'cashflow_w36'=>'Cashflow W36',
  'cashflow_w37'=>'Cashflow W37',
  'cashflow_w38'=>'Cashflow W38',
  'cashflow_w39'=>'Cashflow W39',
  'cashflow_w40'=>'Cashflow W40',
  'cashflow_w41'=>'Cashflow W41',
  'cashflow_w42'=>'Cashflow W42',
  'cashflow_w43'=>'Cashflow W43',
  'cashflow_w44'=>'Cashflow W44',
  'cashflow_w45'=>'Cashflow W45',
  'cashflow_w46'=>'Cashflow W46',
  'cashflow_w47'=>'Cashflow W47',
  'cashflow_w48'=>'Cashflow W48',
  'cashflow_w49'=>'Cashflow W49',
  'cashflow_w50'=>'Cashflow W50',
  'cashflow_w51'=>'Cashflow W51',
  'cashflow_w52'=>'Cashflow W52',
  'cashflow_w53'=>'Cashflow W53',
  'cashflow_w54'=>'Cashflow W54',
  'cashflow_w55'=>'Cashflow W55',
  'cashflow_w56'=>'Cashflow W56',
  'cashflow_w57'=>'Cashflow W57',
  'cashflow_w58'=>'Cashflow W58',
  'cashflow_w59'=>'Cashflow W59',
  'cashflow_w60'=>'Cashflow W60',
  'confirm'=>'Confirm',
  'confirmedby'=>'Confirmed By',
  'timestamp'=>'Timestamp'
);

const ARRAY_WK_MEDICATIONS_PRE = array(
  'id'=>'Id',
  'patient_id'=>'Patient Id',
  'encounter_id'=>'Encounter Id',
  'rec_id'=>'Rec Id',
  'pre_medication'=>'Pre Medication',
  'pre_ingredient'=>'Pre Ingredient',
  'pre_txnumber'=>'Pre Txnumber',
  'pre_date'=>'Rx Date',
  'pre_start_date'=>'Pre Start Date',
  'pre_duration'=>'Pre Duration',
  'pre_duration_unit'=>'Pre Duration Unit',
  'pre_stop_date'=>'Pre Stop Date',
  'pre_start_reason'=>'Pre Start Reason',
  'pre_stop_reason'=>'Pre Stop Reason',
  'pre_indication'=>'Pre Indication',
  'pre_dosage'=>'Pre Dosage',
  'pre_dosage_unit'=>'Pre Dosage Unit',
  'pre_frequency'=>'Pre Frequency',
  'pre_frequency_every'=>'Pre Frequency Every',
  'pre_frequency_mult'=>'Pre Frequency Mult',
  'pre_route'=>'Pre Route',
  'pre_active'=>'Pre Active',
  'add2pmh'=>'Add To PMH',
  'pre_notes'=>'Pre Notes',
  'pre_rxby'=>'Pre Rxby',
  'pre_verified_by'=>'Verified By',
  'pre_verified_date'=>'Verified Date',
  'createdby'=>'Created By',
  'timestamp'=>'Timestamp',
);


function build_query(&$data) {
  $tables = $data['table'];
  $categories = $data['category'];
  $operands = &$data['operand'];
  $values = &$data['value'];
  $join_tables = array_diff(array_unique($tables), array('person'));
  $orderby = 'person.lname, person.fname';

  $select = array(
    'person.lname',
    'person.fname',
    'person.sex',
    'person.htel',
    'person.phn',
    'person.bday',
  );
  // gather all the field we're going to select
  for ($i = 0; $i < sizeof($tables); $i++) {
    $select[] = get_select_field($tables[$i], $categories[$i]);
  }
  $select = array_unique($select);

  $sql = "SELECT SQL_CALC_FOUND_ROWS " . implode(', ', $select) . ", person.id AS adept_patient_id";
  $sql .= " FROM person";
  foreach ($join_tables as $join_table) {
    $sql .= generate_join_sql($join_table);
  }

  $where = "1 = 1";
  for ($i = 0; $i < sizeof($tables); $i++) {
    $where .= generate_where_sql($data, $i);
    if ($tables[$i] == 'apt_reasons') {
      $operands[$i] = '=';
      $values[$i] = $categories[$i];
    }
    if (is_numeric($values[$i])) {
      settype($values[$i], "double");
    }
  }
  $sql .= " WHERE $where GROUP BY (person.id)";
  $sql .= " ORDER BY $orderby";

  return $sql;
}

function get_category_list($table) {
  $options = array();
  switch ($table) {
    case 'laboratory':
      $sql = "SELECT * FROM wk_observation_group GROUP BY name ORDER BY name ASC";
      $result = altSQLStatement($sql);
      while ($row = sqlFetchArray($result)) {
        $options[] = array(
          'id' => '(G)' . $row['name'],
          'text' => '(Group) ' . $row['name']
        );
      }

      $sql = "SELECT * FROM wk_observation_id WHERE observation_id not in (SELECT observation_id FROM wk_observation_group) ORDER BY observation_name";
      $result = altSQLStatement($sql);
      while ($row = sqlFetchArray($result)) {
        $options[] = array(
          'id' => $row['observation_id'],
          'text' => $row['observation_name']
        );
      }
      break;
    case 'person':
    case 'wk_medications_pre':
    case 'wk_genoBiopsy':
    case 'wk_incohrs':
      $sql = "SHOW COLUMNS FROM $table";
      $result = altSQLStatement($sql);
      while ($row = sqlFetchArray($result)) {
        if ($row['Field'] == 'id') {
          continue;
        }
        $options[] = array(
          'id' => $row['Field'],
          'text' => constant("ARRAY_" . strtoupper($table))[$row['Field']]
        );
      }
      break;
    case 'wk_diagnosis':
      $options = array(
        0 => array(
          "id" => "title_icd",
          "text" => "Description"
        ),
        1 => array(
          "id" => "date",
          "text" => "Date"
        ),
        2 => array(
          "id" => "diagnosed_by",
          "text" => "Diagnosed By"
        )
      );
      break;
    case 'wk_problem':
      $options = array(
        0 => array(
          "id" => "problem_type_id",
          "text" => "Problem Name"
        ),
        1 => array(
          "id" => "reported_date",
          "text" => "Reported Date"
        ),
        2 => array(
          "id" => "reported_by",
          "text" => "Report By"
        )
      );
      break;
    case 'role':
      $sql = "SELECT * FROM $table ORDER BY name";
      $result = altSQLStatement($sql);
      while ($row = sqlFetchArray($result)) {
        $options[] = array(
          'id' => $row['id'],
          'text' => $row['name']
        );
      }
      break;
    case 'apt_reasons':
      $sql = "SELECT * FROM $table ORDER BY reason ASC";
      $result = altSQLStatement($sql);
      while ($row = sqlFetchArray($result)) {
        $options[] = array(
          'id' => $row['id'],
          'text' => $row['reason']
        );
      }
      break;
  }
  return $options;
}

function get_category_full_name($table, $key) {
  $name = '';
  switch ($table) {
    case 'laboratory':
      $key = str_replace('(G)', '', $key, $count);
      if ($count) {
        $name = $key;
      } else {
        $sql = "SELECT observation_name FROM wk_observation_id WHERE observation_id = ?";
        $result = altSQLStatement($sql, array($key));
        while ($row = sqlFetchArray($result)) {
          $name = $row['observation_name'];
        }
      }
      break;
    case 'person':
    case 'wk_medications_pre':
    case 'wk_genoBiopsy':
    case 'wk_incohrs':
      $name = constant("ARRAY_" . strtoupper($table))[$key];
      break;
    case 'wk_diagnosis':
      $options = array(
        "title_icd" => "Diagnosis",
        "date" => "Date",
        "diagnosed_by" => "Diagnosed By"
      );
      $name = $options[$key];
      break;
    case 'wk_problem':
      $options = array(
        "problem_type_id" => "Problem Name",
        "reported_date" => "Reported Date",
        "reported_by" => "Report By"
      );
      $name = $options[$key];
      break;
    case 'role':
      $sql = "SELECT name FROM $table WHERE id = ?";
      $result = altSQLStatement($sql, array($key));
      while ($row = sqlFetchArray($result)) {
        $name = $row['name'];
      }
      break;
    case 'apt_reasons':
      $sql = "SELECT reason FROM $table WHERE id = ?";
      $result = altSQLStatement($sql, array($key));
      while ($row = sqlFetchArray($result)) {
        $name = $row['reason'];
      }
      break;
  }
  return $name;
}

function get_select_field($table, $key) {
  switch ($table) {
    case 'laboratory':
      $field = "GROUP_CONCAT(obx_observation_value ORDER BY obx_observation_date DESC SEPARATOR '||') AS laboratory_value";
      break;
    case 'apt_reasons':
      $field = "IF(LENGTH(reason) > 0, 'Yes', 'No') AS apt_value";
      break;
    default:
      $field = "$table.$key";
      break;
  }
  return $field;
}

function get_value_field($table, $key) {
  switch ($table) {
    case 'laboratory':
      $field = "laboratory_value";
      break;
    case 'apt_reasons':
      $field = "apt_value";
      break;
    case 'wk_diagnosis':
      if ($key == 'title_icd') {
        $field = 'description';
      }
      break;
    case 'wk_problem':
      if ($key == 'problem_type_id') {
        $field = 'name';
      }
      break;
    default:
      $field = $key;
      break;
  }
  return $field;
}

function generate_join_sql($join_table) {
  switch ($join_table) {
    case 'laboratory':
      $sql = " LEFT JOIN lab_msh_det ON lab_msh_det.person = CONCAT('{person,', person.id, '}')
        INNER JOIN lab_obx ON lab_obx.lab_msh_id = lab_msh_det.id";
      break;
    case 'wk_medications_pre':
      $sql = " LEFT JOIN $join_table ON $join_table.patient_id = person.id";
      break;
    case 'wk_genoBiopsy':
    case 'wk_incohrs':
      $sql = " LEFT JOIN $join_table ON $join_table.adept_id = person.id";
      break;
    case 'wk_problem':
      $sql = " LEFT JOIN $join_table ON $join_table.person_id = person.id
        INNER JOIN problem_type ON problem_type.id = $join_table.problem_type_id";
      break;
    case 'role':
      $sql = " LEFT JOIN role_relations ON role_relations.person_id = person.id
        INNER JOIN $join_table ON role_relations.role_id = $join_table.id";
      break;
    case 'wk_diagnosis':
      $sql = " LEFT JOIN $join_table ON $join_table.person_id = person.id
        INNER JOIN wk_icdTable ON wk_icdTable.icdCode = $join_table.title_icd";
      break;
    case 'apt_reasons':
      $sql = " LEFT JOIN apt ON apt.patient = person.id";
      break;
  }

  return $sql;
}

function generate_where_sql($data, $index) {
  $andor = $data['andor'];
  $tables = $data['table'];
  $categories = $data['category'];
  $operands = $data['operand'];
  $values = $data['value'];

  switch ($tables[$index]) {
    case 'laboratory':
      $name = str_replace('(G)', '', $categories[$index], $count);
      if ($count > 0) {
        $sql = sprintf(" $andor[$index] (lab_obx.obx_observation_id IN (SELECT observation_id FROM wk_observation_group WHERE name = '%s')
          AND lab_obx.obx_observation_value $operands[$index] ?)", $name);
      } else {
        $sql = sprintf(" $andor[$index] (lab_obx.obx_observation_id = '%s'
          AND lab_obx.obx_observation_value $operands[$index] ?)", $name);
      }
      break;
    case 'role':
      $sql = " $andor[$index] person.type = ?";
      break;
    case 'apt_reasons':
      $sql = " $andor[$index] apt.reason = ?";
      break;
    case 'wk_diagnosis':
      if (stripos($values[$index], 'JFINC') === 0) {
        $categories[$index] = 'jfinc_code';
      }
    default:
      $sql = " $andor[$index] $tables[$index].$categories[$index] $operands[$index] ?";
      break;
  }

  return $sql;
}

function get_autocomplete_options($table, $category) {
  $sql = '';
  $options = array();
  switch ($table) {
    case 'wk_diagnosis':
      switch ($category) {
        case 'title_icd':
          $sql = "SELECT DISTINCT * FROM wk_icdTable ORDER BY description ASC";
          break;
      }

      if ($sql) {
        $result = altSQLStatement($sql);
        while ($row = sqlFetchArray($result)) {
          $code = $row['icdCode'] or $code = $row['jfincCode'] or $code = $row['bcmspCode'] or $code = $row['otherCode'];
          $options[] = array(
            'id' => $code,
            'text' => "$code - " . $row['description']
          );
        }
      }
      break;
    case 'wk_problem':
      switch ($category) {
        case 'problem_type_id':
          $sql = "SELECT DISTINCT * FROM problem_type ORDER BY name ASC";
          break;
      }

      if ($sql) {
        $result = altSQLStatement($sql);
        while ($row = sqlFetchArray($result)) {
          $code = $row['id'];
          $options[] = array(
            'id' => $code,
            'text' => "$code - " . $row['name']
          );
        }
      }
      break;
  }

  return $options;
}

?>
