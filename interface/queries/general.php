<?php
/**
 * Add new user.
 *
 * @package   OpenEMR
 * @link      http://www.open-emr.org
 * @author    Brady Miller <brady.g.miller@gmail.com>
 * @copyright Copyright (c) 2017 Brady Miller <brady.g.miller@gmail.com>
 * @license   https://github.com/openemr/openemr/blob/master/LICENSE GNU General Public License 3
 */

require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/erx_javascript.inc.php");
//require_once("table_definitions.inc");

use OpenEMR\Core\Header;

$alertmsg = '';

?>
<html>
<head>

<?php Header::setupHeader(['common','datetime-picker', 'jquery-ui']); ?>
<title><?php echo xlt("General Queries"); ?></title>

<!-- validation library -->
<!--//Not lbf forms use the new validation, please make sure you have the corresponding values in the list Page validation-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script language="JavaScript">

function trimAll(sString)
{
    while (sString.substring(0,1) == ' ')
    {
        sString = sString.substring(1, sString.length);
    }
    while (sString.substring(sString.length-1, sString.length) == ' ')
    {
        sString = sString.substring(0,sString.length-1);
    }
    return sString;
}

// This invokes the patient search dialog.
function searchme() {
  // temporarily enable first and-or radio buttons
  // and disabled operand dropdowns for serialization
  let $andor = $('input[name^=andor]:disabled');
  $andor.prop('disabled', false);
  let $operand = $('select[name^=operand]:disabled');
  $operand.prop('disabled', false);

  let data = $('form#query_general').serialize();
  $andor.prop('disabled', true);
  $operand.prop('disabled', true);

  let url = './results.php?' + data;

  dlgopen(url, '_blank', 700, 500);
}
function authorized_clicked() {
     var f = document.forms[0];
     f.calendar.disabled = !f.authorized.checked;
     f.calendar.checked  =  f.authorized.checked;
}

</script>
<script language="JavaScript">
  $(document).ready(function() {
      $('select#category').select2({
        placeholder: "Select query table",
        width: '100%',
        theme: "bootstrap"
      });
      $('#search').click(function() { searchme(); });
      $('#reset').click(function() {
        reset_query_form();
      });

      $('select#table').change(function() {
        change_query_table(this);
      });

      $('button#add-row').click(function() {
        let tr = $('table.query.general tbody tr:first').clone();
        let num_rows = $('table.query.general tbody tr').length;

        // reset all values to be ready to be inserted to the form
        $('input[name^=andor]', tr).attr('name', "andor[" + num_rows + "]");
        $('input[name^=andor]', tr).prop('disabled', false);
        $('.and-or', tr).removeClass('hidden');

        $('input#value', tr).val('');
        $('span.select2', tr).remove(); // remove dangling select2 dropdown
        $('select#operand', tr).prop('disabled', false);
        $('input#value', tr).prop('disabled', false);
        $('input#value', tr).show();

        $('select#category option', tr).remove();
        $('select#category', tr).siblings().remove();
        $('select#category', tr).select2({
          placeholder: "Select query table",
          width: '100%',
          theme: "bootstrap",
        });

        // add listeners
        $('button.remove-row', tr).on('click', function() {
          remove_row(this);
          return false;
        });
        $('select#table', tr).on('change', function() {
          change_query_table(this);
        });

        // finally we append the row to the bottom
        $('table.query.general tbody tr:last').after(tr);
        return false;
      });

      $('button.remove-row').click(function() {
        remove_row(this);
        return false;
      });


      function remove_row(button) {
        let $all_tr = $('table.query.general tbody tr');
        if ($all_tr.length > 1) {
          let $this_tr = $(button).closest('tr');
          let index = $all_tr.index($this_tr);
          for (var i = index+1; i < $all_tr.length; i++) {
            $('input[name^=andor]', $all_tr.eq(i)).attr('name', "andor[" + (i-1) + "]");

            // hide second row's andor radios as it'll become the first
            if (i==1) {
              $('.and-or', $all_tr.eq(i)).addClass('hidden');
              $('input[name^=andor][value=and]', $all_tr.eq(i)).prop('checked', true);
              $('input[name^=andor]', $all_tr.eq(i)).prop('disabled', true);
            }
          }

          $this_tr.remove();
        } else {
          reset_query_form();
        }
      }

      function reset_query_form() {
        $('form#query_general').get(0).reset();
        $('select#table').each(function() {
          change_query_table(this);
        });
      }

      function change_query_table(query_table) {
        var row = $(query_table).closest('tr');
        $.get({
          url: 'table_definitions.php',
          data: {
            table: $(query_table).val()
          },
          success: function(result) {
            $('select#category option', row).remove();
            $('select#category', row).select2({
              placeholder: "Select query table",
              width: '100%',
              theme: "bootstrap",
              data: result
            });

            if ($.inArray($(query_table).val(), ['role', 'apt_reasons']) > -1) {
              $('select#operand', row).val('=');
              $('select#operand', row).prop('disabled', true);

              $('input#value', row).val('');
              $('input#value', row).prop('disabled', true);
            } else {
              // assign listener to switch to autocomplete dropdown if needed
              generate_autocomplete($('input#value', row));
              $('select#category', row).on('change', function() {
                generate_autocomplete(this);
              });
            }
          }
        });
      }

      function generate_autocomplete(option) {
        var row = $(option).closest('tr');
        $.get({
          url: 'value_autocomplete.php',
          data: {
            table: $("select#table", row).val(),
            category: $("select#category", row).val()
          },
          success: function(result) {
            if (!$.isEmptyObject(result)) {
              $('input#value', row).prop('disabled', true);
              $('input#value', row).hide();

              $('select#operand', row).val('=');
              $('select#operand', row).prop('disabled', true);

              $('select#value', row).prop('disabled', false);
              $('select#value option', row).remove();
              $('select#value', row).select2({
                width: '100%',
                theme: "bootstrap",
                data: result
              });
              $('select#value', row).show();
            } else {
              if ($('select#value', row).hasClass("select2-hidden-accessible")) {
                $('select#value', row).select2('destroy');
              }
              $('select#value', row).prop('disabled', true);
              $('select#value', row).hide();

              $('select#operand', row).prop('disabled', false);

              $('input#value', row).prop('disabled', false);
              $('input#value', row).show();
            }

          }
        });
      }
  });
</script>

<style type="text/css">
  table.query th,
  table.query td{
    padding: 5px;
    max-width: 500px;
  }
</style>
</head>

<body class="body_top">

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h2><?php echo xlt('General Queries');?></h2>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-2 col-xs-offset-10 btn-group">
                    <button type="button" class="btn btn-default btn-search" id="search" value="<?php echo xla('Search'); ?>">
                        <?php echo xlt('Search'); ?>
                    </button>
                    <button type="button" class="btn btn-default btn-cancel" name='reset' id="reset" value="<?php echo xla('Reset'); ?>">
                        <?php echo xlt('Reset'); ?>
                    </button>
                </div>
                <hr>
            </div>
        </div>
    </div>

<br><br>

<table border=0 width="100%">

<tr><td valign=top>
<form name='query_general' id="query_general" method='post' action="general.php">
<input type='hidden' name='mode' value='query_general'>

<span class="bold">&nbsp;</span>
<table class="query general" border=0 style="width:80%;margin:0 auto;">
  <thead>
    <tr>
      <th></th>
      <th><span class="text"><?php echo xlt('Query Table'); ?></span></th>
      <th><span class="text"><?php echo xlt('Category'); ?></span></th>
      <th><span class="text"><?php echo xlt('Operand'); ?></span></th>
      <th><span class="text"><?php echo xlt('Value'); ?></span></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: right;">
        <div class="and-or hidden">
          <input type="radio" name='andor[0]' value='and' disabled checked> and
          <input type="radio" name='andor[0]' value='or'  disabled> or
        </div>
      </td>
      <td style="">
        <select name='table[]' id='table' style="" class="form-control">
          <option value="" disabled selected><?php echo xl("Select query table"); ?></option>
          <option value="laboratory">Laboratory</option>
          <option value="person">Person</option>
          <option value="wk_medications_pre">Medication</option>
          <option value="wk_genoBiopsy">GenoBiopsy</option>
          <option value="wk_diagnosis">Diagnosis</option>
          <option value="wk_problem">Problem</option>
          <option value="wk_incohrs">Incohrs</option>
          <option value="role">Role</option>
          <option value="apt_reasons">Appointment Reason</option>
        </select>
      </td>
      <td>
        <select name='category[]' id='category' style="" class="form-control" />
      </td>
      <td>
        <select name='operand[]' id='operand' style="" class="form-control">
          <option value=">">></option>
          <option value=">=">>=</option>
          <option selected value="=">=</option>
          <option value="!=">!=</option>
          <option value="<"><</option>
          <option value="<="><=</option>
        </select>
      </td>
      <td>
        <input type=entry name='value[]' id='value' style="" class="form-control">
        <select name='value[]' id='value' style="display:none;" class="form-control" disabled>
      </td>
      <td style="width:40px;">
        <button class="btn btn-default btn-cancel remove-row"></button>
      </td>
    </tr>
  </tbody>
  <tfoot>
    <tr style="height: 75px;vertical-align: bottom;">
      <td style="width:40px;">
        <button class="btn btn-default btn-add" id="add-row"><?php echo xlt('Add Criteria'); ?></button>
      </td>
    </tr>
  </tfoot>
</table>

<br>
<input type="hidden" name="newauthPass">
</form>
</td>

</tr>




</table>

<script language="JavaScript">
<?php
if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
}
?>
$(document).ready(function(){
    $("#cancel").click(function() {
          dlgclose();
     });

});
</script>
<table>

</table>

</body>
</html>
