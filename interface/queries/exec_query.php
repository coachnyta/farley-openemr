<?php
/**
 * Query results screen.
 *
 * @package OpenEMR
 * @link http://www.open-emr.org
 * @license https://github.com/openemr/openemr/blob/master/LICENSE GNU General Public License 3
 */

require_once("../globals.php");
require_once("$srcdir/options.inc.php");
require_once(dirname(__FILE__) . "/../../vendor/php-paginator-master/src/JasonGrimes/Paginator.php");
require_once(dirname(__FILE__) . "/../../library/queries/sql.inc");
require_once(dirname(__FILE__) . "/functions.inc");
//require_once("table_definitions.inc");

use JasonGrimes\Paginator;

$MAXSHOW = 100; // maximum number of results to display at once

$data = $_GET;
$tables = $data['table'];
$categories = $data['category'];
$operands = &$data['operand'];
$values = &$data['value'];


$current_page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$orderby = 'person.lname, person.fname';
$sqllimit = $MAXSHOW;
$count = 0;

$fstart = ($current_page-1) * $sqllimit;
$fend = $fstart + $sqllimit;

unset($data["fstart"]);
unset($data["page"]);

$output = array(
  "columns" => array(
    xl('Name'),
    xl('Sex'),
    xl('Phone'),
    xl('PHN'),
    xl('DOB')
  ),
  "data" => array()
);

$other_categories = array();
for ($index = 0; $index < sizeof($categories); $index++) {
  $other_categories[] = array(
    "table" => $tables[$index],
    "category" => $categories[$index]
  );
}
$skipped_categories = array('fname', 'lname', 'sex', 'wtel', 'htel', 'mtel', 'phn', 'bday');

foreach ($other_categories as $cat) {
  if (in_array($cat["category"], $skipped_categories)) continue;

  $fullname = get_category_full_name($cat["table"], $cat["category"]);
  $output["columns"][] = xl($fullname);
}


$sql = build_query($data);
$sql .= " LIMIT " . escape_limit($fstart) . ", " . escape_limit($sqllimit);
$output['sql'] = $sql;

$rez = altSQLStatement($sql, $values);
while ($row = sqlFetchArray($rez)) {
  $adept_profile_url = $GLOBALS['adept']['hostname'] . "patients/basic/?p=de&preid=&aid=" . $row['adept_patient_id'];
  $full_name = $row['lname'] . ", " . $row['fname'];

  $output_values = array(
    'link' => $adept_profile_url,
    'values' => array(
      $full_name,
      $row['sex'],
      $row['htel'],
      $row['phn'],
      ($row["bday"] != "0000-00-00 00:00:00") ? $row["bday"] : ''
    )
  );

  foreach ($other_categories as $cat) {
    if (in_array($cat["category"], $skipped_categories)) continue;

    $value_field = get_value_field($cat["table"], $cat["category"]);
    $output_values['values'][] = $row[$value_field];
  }

  $output["data"][] = $output_values;
}

$count_sql = "SELECT FOUND_ROWS() AS count";
$rez = altSQLStatement($count_sql);
$count = sqlFetchArray($rez);
$output["count"] = $count["count"];

if ($count["count"]) {
  $countStatement =  " - " . min($fend, $count["count"]) . " " . xl('of') . " " . $count["count"];
  $output["count_statement"] = ($fstart + 1) . htmlspecialchars($countStatement, ENT_NOQUOTES);
}

//print "<pre>"; print_r($data); print "</pre>"; die;
$url_pattern = "results.php?" . http_build_query($data) . "&page=(:num)";
$paginator = new Paginator($count["count"], $sqllimit, $current_page, $url_pattern);
$output["paginator"] = $paginator->__toString();

header('Content-Type: application/json');
echo json_encode($output);

?>
