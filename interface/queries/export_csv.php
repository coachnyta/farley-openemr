<?php
/**
 * Query results screen.
 *
 * @package OpenEMR
 * @link http://www.open-emr.org
 * @license https://github.com/openemr/openemr/blob/master/LICENSE GNU General Public License 3
 */

require_once("../globals.php");
require_once("$srcdir/options.inc.php");
require_once(dirname(__FILE__) . "/../../library/queries/sql.inc");
require_once(dirname(__FILE__) . "/functions.inc");

$data = $_GET;
$tables = $data['table'];
$categories = $data['category'];
$operands = &$data['operand'];
$values = &$data['value'];

$other_categories = array();
for ($index = 0; $index < sizeof($categories); $index++) {
  $other_categories[] = array(
    "table" => $tables[$index],
    "category" => $categories[$index]
  );
}
$skipped_categories = array('fname', 'lname', 'sex', 'wtel', 'htel', 'mtel', 'phn', 'bday');


if ($_GET["mode"] == 'query_general') {
  $csv = '';
  $sql = build_query($data);

  $rez = altSQLStatement($sql, $values);
  $result = array();
  while ($row = sqlFetchArray($rez)) {
    $result[] = $row;
  }

  $csv  = '"' . xl('Name') . '", '
        . '"' . xl('Sex') . '", '
        . '"' . xl('Phone') . '", '
        . '"' . xl('PHN') . '", '
        . '"' . xl('DOB') . '"';
  foreach ($other_categories as $cat) {
    if (in_array($cat["category"], $skipped_categories)) continue;

    $fullname = get_category_full_name($cat["table"], $cat["category"]);
    $csv .= ', "' . xl($fullname) . '"';
  }
  $csv .= "\n";

  $rez = altSQLStatement($sql, $values);
  while ($row = sqlFetchArray($rez)) {
    $csv .= '"' . $row['lname'] . ", " . $row['fname'] . '", '
          . '"' . $row['sex'] . '", '
          . '"' . $row['htel'] . '", '
          . '"' . $row['phn'] . '", '
          . '"' . $row['bday'] . '"';
    foreach ($other_categories as $cat) {
      if (in_array($cat["category"], $skipped_categories)) continue;

      $value_field = get_value_field($cat["table"], $cat["category"]);
      $value = preg_replace('/\|\|/', "", $row[$value_field]);
      $csv .= ', "' . $value . '"';
    }
    $csv .= "\n";
  }
}

  header("Content-type: text/csv" );
  header("Content-Disposition: attachment; filename=QueryResult.csv");
  header("Pragma: no-cache");
  header("Expires: 0");
  echo $csv;

?>
