<?php
/**
* This is a mere copypasta of library/sql.inc
* for the purpose of enabling a secondary mysql connection
*/
/**
* Sql functions/classes for OpenEMR.
*
* Includes classes and functions that OpenEMR uses
* to interact with SQL.
*
* LICENSE: This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://opensource.org/licenses/gpl-license.php>.
*
* @package   OpenEMR
* @link      http://www.open-emr.org
*/

require_once(dirname(__FILE__) . "/../sqlconf.php");
require_once(dirname(__FILE__) . "/../../vendor/adodb/adodb-php/adodb.inc.php");
require_once(dirname(__FILE__) . "/../../vendor/adodb/adodb-php/drivers/adodb-mysqli.inc.php");
require_once(dirname(__FILE__) . "/../ADODB_mysqli_log.php");

if (!defined('ADODB_FETCH_ASSOC')) {
    define('ADODB_FETCH_ASSOC', 2);
}

$altDB = NewADOConnection("mysqli_log"); // Use the subclassed driver which logs execute events
// Below optionFlags flag is telling the mysql connection to ensure local_infile setting,
// which is needed to import data in the Administration->Other->External Data Loads feature.
// (Note the MYSQLI_READ_DEFAULT_GROUP is just to keep the current setting hard-coded in adodb)
$altDB->optionFlags = array(array(MYSQLI_READ_DEFAULT_GROUP,0), array(MYSQLI_OPT_LOCAL_INFILE,1));
// Set mysql to use ssl, if applicable.
// Can support basic encryption by including just the mysql-ca pem (this is mandatory for ssl)
// Can also support client based certificate if also include mysql-cert and mysql-key (this is optional for ssl)
if (file_exists($GLOBALS['OE_SITE_DIR'] . "/documents/certificates/mysql-ca")) {
    if (defined('MYSQLI_CLIENT_SSL')) {
        $altDB->clientFlags = MYSQLI_CLIENT_SSL;
    }
}
$altDB->port = $adeptDBconf["port"];
$altDB->PConnect($adeptDBconf["host"], $adeptDBconf["login"], $adeptDBconf["pass"], $adeptDBconf["dbase"]);
$GLOBALS['altdb']['db'] = $altDB;
$GLOBALS['altdbh'] = $altDB->_connectionID;

// Modified 5/2009 by BM for UTF-8 project ---------
if (!$disable_utf8_flag) {
    $success_flag = $altDB->Execute("SET NAMES 'utf8'");
    if (!$success_flag) {
        error_log("PHP custom error: from openemr library/sql.inc  - Unable to set up UTF8 encoding with mysql database: ".getAltSQLLastError(), 0);
    }
}

// Turn off STRICT SQL
$sql_strict_set_success = $altDB->Execute("SET sql_mode = ''");
if (!$sql_strict_set_success) {
    error_log("Unable to set strict sql setting: ".getAltSQLLastError(), 0);
}

// set up associations in adodb calls (not sure why above define
//  command does not work)
$GLOBALS['altdb']['db']->SetFetchMode(ADODB_FETCH_ASSOC);

if ($GLOBALS['debug_ssl_mysql_connection']) {
    error_log("CHECK SSL CIPHER IN MAIN ADODB: " . print_r($GLOBALS['altdb']['db']->ExecuteNoLog("SHOW STATUS LIKE 'Ssl_cipher';")->fields, true));
}

//fmg: This makes the login screen informative when no connection can be made
if (!$GLOBALS['altdbh']) {
  //try to be more helpful
    if ($host == "localhost") {
        echo "Check that mysqld is running.<p>";
    } else {
        echo "Check that you can ping the server " . text($adeptDBconf["host"]) . ".<p>";
    }//if local
    HelpfulDie("Could not connect to server!", getAltSQLLastError());
    exit;
}//if no connection

/**
* Standard sql query in OpenEMR.
*
* Function that will allow use of the adodb binding
* feature to prevent sql-injection. Will continue to
* be compatible with previous function calls that do
* not use binding.
* It will return a recordset object.
* The sqlFetchArray() function should be used to
* utilize the return object.
*
* @param  string  $statement  query
* @param  array   $binds      binded variables array (optional)
* @return recordset
*/
function altSQLStatement($statement, $binds = false)
{
  // Below line is to avoid a nasty bug in windows.
    if (empty($binds)) {
        $binds = false;
    }

  // Use adodb Execute with binding and return a recordset.
  //   Note that the auditSQLEvent function is embedded
  //    in the Execute command.
    $recordset = $GLOBALS['altdb']['db']->Execute($statement, $binds);
    if ($recordset === false) {
        HelpfulDie("query failed: $statement", getAltSQLLastError());
    }

    return $recordset;
}

/**
* Specialized sql query in OpenEMR that skips auditing.
*
* Function that will allow use of the adodb binding
* feature to prevent sql-injection. Will continue to
* be compatible with previous function calls that do
* not use binding. It is equivalent to the
* sqlStatement() function, EXCEPT it skips the
* audit engine. This function should only be used
* in very special situations.
* It will return a recordset object.
* The sqlFetchArray() function should be used to
* utilize the return object.
*
* @param  string  $statement  query
* @param  array   $binds      binded variables array (optional)
* @return recordset
*/
function altSQLStatementNoLog($statement, $binds = false)
{
  // Below line is to avoid a nasty bug in windows.
    if (empty($binds)) {
        $binds = false;
    }

  // Use adodb ExecuteNoLog with binding and return a recordset.
    $recordset = $GLOBALS['altdb']['db']->ExecuteNoLog($statement, $binds);
    if ($recordset === false) {
        HelpfulDie("query failed: $statement", getAltSQLLastError());
    }

    return $recordset;
}


/**
 * Wrapper for ADODB getAssoc
 *
 * @see http://adodb.org/dokuwiki/doku.php?id=v5:reference:connection:getassoc
 *
 * @param string $sql
 * @param string[] $bindvars
 * @param boolean $forceArray
 * @param boolean $first2Cols
 * @return array
 */
function altSQLGetAssoc($sql, $bindvars = false, $forceArray = false, $first2Cols = false)
{

    return $GLOBALS['altdb']['db']->getAssoc($sql, $bindvars, $forceArray, $first2Cols);
}

/**
* Specialized sql query in OpenEMR that only returns
* the first row of query results as an associative array.
*
* Function that will allow use of the adodb binding
* feature to prevent sql-injection.
*
* @param  string  $statement  query
* @param  array   $binds      binded variables array (optional)
* @return array
*/
function altSQLQuery($statement, $binds = false)
{
  // Below line is to avoid a nasty bug in windows.
    if (empty($binds)) {
        $binds = false;
    }

    $recordset = $GLOBALS['altdb']['db']->Execute($statement, $binds);

    if ($recordset === false) {
        HelpfulDie("query failed: $statement", getAltSQLLastError());
    }

    if ($recordset->EOF) {
        return false;
    }

    $rez = $recordset->FetchRow();
    if ($rez == false) {
        return false;
    }

    return $rez;
}

/**
* Specialized sql query in OpenEMR that bypasses the auditing engine
* and only returns the first row of query results as an associative array.
*
* Function that will allow use of the adodb binding
* feature to prevent sql-injection. It is equivalent to the
* sqlQuery() function, EXCEPT it skips the
* audit engine. This function should only be used
* in very special situations.
*
* @param  string  $statement  query
* @param  array   $binds      binded variables array (optional)
* @return array
*/
function altSQLQueryNoLog($statement, $binds = false)
{
  // Below line is to avoid a nasty bug in windows.
    if (empty($binds)) {
        $binds = false;
    }

    $recordset = $GLOBALS['altdb']['db']->ExecuteNoLog($statement, $binds);

    if ($recordset === false) {
        HelpfulDie("query failed: $statement", getAltSQLLastError());
    }

    if ($recordset->EOF) {
        return false;
    }

    $rez = $recordset->FetchRow();
    if ($rez == false) {
        return false;
    }

    return $rez;
}

/**
* Specialized sql query in OpenEMR that ignores sql errors, bypasses the
* auditing engine and only returns the first row of query results as an
* associative array.
*
* Function that will allow use of the adodb binding
* feature to prevent sql-injection. It is equivalent to the
* sqlQuery() function, EXCEPT it skips the
* audit engine and ignores erros. This function should only be used
* in very special situations.
*
* @param  string  $statement  query
* @param  array   $binds      binded variables array (optional)
* @return array
*/
function altSQLQueryNoLogIgnoreError($statement, $binds = false)
{
  // Below line is to avoid a nasty bug in windows.
    if (empty($binds)) {
        $binds = false;
    }

    $recordset = $GLOBALS['altdb']['db']->ExecuteNoLog($statement, $binds);

    if ($recordset === false) {
        // ignore the error and return FALSE
        return false;
    }

    if ($recordset->EOF) {
        return false;
    }

    $rez = $recordset->FetchRow();
    if ($rez == false) {
        return false;
    }

    return $rez;
}

/**
* Function that will safely return the last error,
* and accounts for the audit engine.
*
* @param   string  $mode either adodb(default) or native_mysql
* @return  string        last mysql error
*/
function getAltSQLLastError()
{
    return $GLOBALS['altdb']['db']->ErrorMsg();
}

/**
 * Function that will safely return the last error no,
 * and accounts for the audit engine.
 *
 * @param   string  $mode either adodb(default) or native_mysql
 * @return  string        last mysql error no
 */
function getAltSQLLastErrorNo()
{
    return $GLOBALS['altdb']['db']->ErrorNo();
}

/**
* Function that will return an array listing
* of columns that exist in a table.
*
* @param   string  $table sql table
* @return  array
*/
function altSQLListFields($table)
{
    $sql = "SHOW COLUMNS FROM ". add_escape_custom($table);
    $resource = sqlQ($sql);
    $field_list = array();
    while ($row = sqlFetchArray($resource)) {
        $field_list[] = $row['Field'];
    }

    return $field_list;
}

