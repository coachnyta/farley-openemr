<?php

require_once(dirname(__FILE__) . "/../../interface/globals.php");
require_once(dirname(__FILE__) . "/../../library/queries/sql.inc");
require_once(dirname(__FILE__) . "/../../library/forms.inc");
require_once(dirname(__FILE__) . "/../../library/encounter.inc");

use OpenEMR\Services\FacilityService;

function import_patient_demographics($source) {
  $result = sqlStatement("SELECT patient_id FROM import_patients WHERE external_id = ?", array($source['id']));
  $dob = DateTime::createFromFormat('Ymd', $source['bday']);
  $sex = get_patient_gender($source['sex']);
  if (!$row = sqlFetchArray($result)) {
    $pid = sqlFetchArray(sqlStatement("SELECT MAX(pid) AS pid FROM patient_data"));
    $insert_id = sqlInsert("INSERT INTO patient_data (pid, title, 
      fname, lname, mname, DOB, street, postal_code, city, state, country_code, 
      ss, phone_home, phone_biz, phone_contact, phone_cell, sex, 
      email, ethnoracial, race, ethnicity, 
      billing_note, deceased_date, deceased_reason)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 
      array(
        $pid['pid'] +1,
        $source['title'],
        $source['fname'],
        $source['lname'],
        $source['mname'],
        $dob ? $dob->format('Y-m-d') : '',
        $source['street1'] . ' ' . $source['street2'],
        $source['pcode'],
        $source['city'],
        $source['province'],
        $source['country'],
        $source['phn'],
        $source['htel'],
        $source['wtel'],
        $source['otel'],
        $source['mtel'],
        $sex,
        $source['email'],
        $source['ethnicity'],
        $source['ethnicity_other'],
        $source['ethnicity'],
        $source['notes_billing'],
        $source['deceased'],
        $source['deceased_cause']
      )
    );
    
    $sql = sqlInsert("INSERT INTO import_patients VALUES (?, ?, ?)", 
            array(
              $insert_id, 
              $source['id'],
              'mditdb'
            )
          );
  } else {
    $sql = sqlStatement("UPDATE patient_data SET 
      title =?, 
      fname = ?, 
      lname = ?, 
      mname = ?, 
      DOB = ?, 
      street = ?, 
      postal_code = ?, 
      city = ?, 
      state = ?, 
      country_code = ?, 
      ss = ?,
      phone_home = ?, 
      phone_biz = ?, 
      phone_contact = ?, 
      phone_cell = ?, 
      sex = ?, 
      email = ?, 
      ethnoracial = ?, 
      race = ?, 
      ethnicity = ?, 
      billing_note = ?, 
      deceased_date = ?, 
      deceased_reason = ?
      WHERE pid = ?", 
      array(
        $source['title'],
        $source['fname'],
        $source['lname'],
        $source['mname'],
        $dob ? $dob->format('Y-m-d') : '',
        $source['street1'] . ' ' . $source['street2'],
        $source['pcode'],
        $source['city'],
        $source['province'],
        $source['country'],
        $source['phn'],
        $source['htel'],
        $source['wtel'],
        $source['otel'],
        $source['mtel'],
        $sex,
        $source['email'],
        $source['ethnicity'],
        $source['ethnicity_other'],
        $source['ethnicity'],
        $source['notes_billing'],
        $source['deceased'],
        $source['deceased_cause'],

        $row['patient_id']
      )
    );
  }
  
  return $sql;
}

// import one encounter
function import_patient_encounters($encounter) {
  $result = sqlStatement("SELECT encounter_id FROM import_encounters WHERE external_id = ?", array($encounter['id']));
  
  $date = NULL;
  $date_raw = DateTime::createFromFormat('Ymd', $encounter['date']);
  if ($date_raw) {
    $date = $date_raw->format('Y-m-d');
  }

  if (!$row = sqlFetchArray($result)) {
    // Get patient who this encounter is belong to
    $result = sqlStatement("SELECT patient_id FROM import_patients WHERE external_id = ?", array($encounter['patient']));
    while ($row = sqlFetchArray($result)) {
      $encounter['pid'] = $row['patient_id'];
    }

    // Save encounter
    $encounter_id = generate_id();
    $insert_id = addForm(
        $encounter_id,
        "New Patient Encounter",
        sqlInsert(
            "INSERT INTO form_encounter SET
                date = ?,      
                onset_date = ?,
                reason = ?,
                facility = ?,
                pc_catid = ?,
                facility_id = ?,
                billing_facility = ?,
                sensitivity = ?,
                referral_source = ?,
                pid = ?,
                encounter = ?,
                pos_code = ?,
                external_id = ?,
                provider_id = ?",
            array(
                $date,
                $date,
                $encounter['reason'],
                $encounter['location_name'],
                5,    // TODO: to support non-visit encounters
                $encounter['loc'],
                $encounter['loc'],
                NULL,
                '',
                $encounter['pid'],
                $encounter_id,
                1,    // TODO: to find out and support pos_code
                NULL,
                1     // TODO: to find out and support provider
            )
        ),
        "newpatient",
        $encounter['pid'],
        1,
        $date,
        'admin',  // hardcoded
        'Default' // hardcoded
    );
    $sql = sqlInsert("INSERT INTO import_encounters VALUES (?, ?, ?)", 
          array(
            $insert_id, 
            $encounter['id'],
            'mditdb'
          )
        );
  } else {
    $sqlBindArray = array();
    
    array_push(
        $sqlBindArray,
        $date,
        $date,
        $encounter['reason'],
        $encounter['location_name'],
        5,    // TODO: to support non-visit encounters
        3,    // TODO: import locations
        3,    // TODO: import locations
        NULL,
        '',
        1,    // TODO: to find out and support pos_code
        $encounter['id']
    );
    $sql = sqlStatement(
        "UPDATE form_encounter SET
            date = ?,
            onset_date = ?,
            reason = ?,
            facility = ?,
            pc_catid = ?,
            facility_id = ?,
            billing_facility = ?,
            sensitivity = ?,
            referral_source = ?,
            pos_code = ? WHERE id = ?",
        $sqlBindArray
    );
  }

  return $sql;
}

function import_facilities($source) {
  $result = sqlStatement("SELECT * FROM facility WHERE name = ?", array($source['name']));
  
  $facilityService = new FacilityService();
  
  $target = array(
    "name" => $source["name"],
    "phone" => $source["tel"],
    "fax" => $source["fax"],
    "street" => $source["street"],
    "city" => $source["city"],
    "state" => $source["province"],
    "postal_code" => $source["pcode"],
    "country_code" => trim(isset($source["country"])) ? $source["country"] : 'Canada',
    "website" => $source["website"],
    "email" => $source["email"],
    
    "mail_street" => $source["street"],
    "mail_city" => $source["city"],
    "mail_state" => $source["province"],
    "mail_zip" => $source["pcode"],
  );

  if (!$row = sqlFetchArray($result)) {
    $target["service_location"] = 1;
    $target["billing_location"] = 1;
    
    $target["federal_ein"] = '';
    $target["website"] = '';
    $target["email"] = '';
    $target["color"] = '#000000';
    $target["accepts_assignment"] = '';
    $target["pos_code"] = '';
    $target["domain_identifier"] = '';
    $target["attn"] = '';
    $target["tax_id_type"] =  '';
    $target["primary_business_entity"] = '';
    $target["facility_npi"] = '';
    $target["facility_taxonomy"] = '';
    $target["facility_code"] = '';
    $target["mail_street2"] = '';
    $target["oid"] = '';

    $facilityService->insert($target);
  } else {
    $target = array_merge($target, $row);
    $target["fid"] = $row["id"];
    
    $facilityService->update($target);
  }
}

function import_patient_labs($date = null) {
  $patient_hash = array();
  $result = sqlStatement("SELECT patient_id FROM import_patients");
  while ($row = sqlFetchArray($result)) {
    $patient_hash[$row['external_id']] = $row['patient_id'];
  }
}

function get_patient_gender($sex) {
  switch ($sex) {
    case 'Male':
    case 'M':
      $result = 'Male';
      break;
    case 'Female':
    case 'F':
      $result = 'Female';
      break;
    case 'N/A':
    default:
      $result = 'N/A';
      break;
  }
  
  return $result;
}