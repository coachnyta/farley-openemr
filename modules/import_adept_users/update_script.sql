/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  coachnyta
 * Created: Apr 3, 2019
 */

CREATE TABLE import_scripts (
  `id` int not null primary key auto_increment,
  `name` varchar(40) UNIQUE,
  `last_run` datetime ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO import_scripts (`name`, `last_run`) VALUES ('adept_patient_import', '1000-01-01 00:00:00');
INSERT INTO import_scripts (`name`, `last_run`) VALUES ('adept_encounter_import', '1000-01-01 00:00:00');

CREATE TABLE import_patients (
  `patient_id` int not null primary key,
  `external_id` int not null,
  `source_db` varchar(40)
);

CREATE TABLE import_encounters (
  `encounter_id` int not null primary key,
  `external_id` int not null,
  `source_db` varchar(40)
);

INSERT INTO list_options (`list_id`, `option_id`, `title`, `seq`)
VALUES ('sex', 'N/A', 'N/A', 3);

SET @catid = (SELECT MAX(pc_catid) FROM  openemr_postcalendar_categories);
INSERT INTO `openemr_postcalendar_categories` (`pc_catid`, `pc_catname`, `pc_constant_id`, `pc_catcolor`, `pc_catdesc`, `pc_recurrtype`, `pc_enddate`, `pc_recurrspec`, `pc_recurrfreq`, `pc_duration`, `pc_end_date_flag`, `pc_end_date_type`, `pc_end_date_freq`, `pc_end_all_day`, `pc_dailylimit`, `pc_cattype`, `pc_active`, `pc_seq`) VALUES (@catid+1, 'Telephone', 'telephone', '#CCCCFF', 'Telephone', 0, NULL, 'a:5:{s:17:"event_repeat_freq";s:1:"0";s:22:"event_repeat_freq_type";s:1:"0";s:19:"event_repeat_on_num";s:1:"1";s:19:"event_repeat_on_day";s:1:"0";s:20:"event_repeat_on_freq";s:1:"0";}', 0, 900, 0, 0, 0, 0, 0,0,1,@catid+1);
SET @catid = (SELECT MAX(pc_catid) FROM  openemr_postcalendar_categories);
INSERT INTO `openemr_postcalendar_categories` (`pc_catid`, `pc_catname`, `pc_constant_id`, `pc_catcolor`, `pc_catdesc`, `pc_recurrtype`, `pc_enddate`, `pc_recurrspec`, `pc_recurrfreq`, `pc_duration`, `pc_end_date_flag`, `pc_end_date_type`, `pc_end_date_freq`, `pc_end_all_day`, `pc_dailylimit`, `pc_cattype`, `pc_active`, `pc_seq`) VALUES (@catid+1, 'Fax', 'fax', '#CCCCFF', 'Fax', 0, NULL, 'a:5:{s:17:"event_repeat_freq";s:1:"0";s:22:"event_repeat_freq_type";s:1:"0";s:19:"event_repeat_on_num";s:1:"1";s:19:"event_repeat_on_day";s:1:"0";s:20:"event_repeat_on_freq";s:1:"0";}', 0, 900, 0, 0, 0, 0, 0,0,1,@catid+1);
SET @catid = (SELECT MAX(pc_catid) FROM  openemr_postcalendar_categories);
INSERT INTO `openemr_postcalendar_categories` (`pc_catid`, `pc_catname`, `pc_constant_id`, `pc_catcolor`, `pc_catdesc`, `pc_recurrtype`, `pc_enddate`, `pc_recurrspec`, `pc_recurrfreq`, `pc_duration`, `pc_end_date_flag`, `pc_end_date_type`, `pc_end_date_freq`, `pc_end_all_day`, `pc_dailylimit`, `pc_cattype`, `pc_active`, `pc_seq`) VALUES (@catid+1, 'Email', 'email', '#CCCCFF', 'Email', 0, NULL, 'a:5:{s:17:"event_repeat_freq";s:1:"0";s:22:"event_repeat_freq_type";s:1:"0";s:19:"event_repeat_on_num";s:1:"1";s:19:"event_repeat_on_day";s:1:"0";s:20:"event_repeat_on_freq";s:1:"0";}', 0, 900, 0, 0, 0, 0, 0,0,1,@catid+1);
