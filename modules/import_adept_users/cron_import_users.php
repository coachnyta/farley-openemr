<?php

////////////////////////////////////////////////////////////////////
// Package:	import_adapt_users
// Purpose:	to be run by cron every day, import and update all the users 
// with the most up-to-date information from the adapt db
//
// Created by:  Hans Lee on 03/03/2019
// Updated by:	Hans Lee on 03/03/2019
////////////////////////////////////////////////////////////////////

// larry :: hack add for command line version
$_SERVER['REQUEST_URI']=$_SERVER['PHP_SELF'];
$_SERVER['SERVER_NAME']='localhost';
$backpic = "";

$_GET['site'] = 'default';

$ignoreAuth=1;
include_once(dirname(__FILE__) . "/../../interface/globals.php");
require_once(dirname(__FILE__) . "/../../library/queries/sql.inc");
require_once("functions.inc");

$result = sqlStatement("SELECT * FROM import_scripts WHERE name = ?", array("adept_patient_import"));
$script = sqlFetchArray($result);

//$result = altSQLStatement("SELECT * FROM person");
//while ($row = sqlFetchArray($result)) {
//  import_patient_demographics($row);
//}

$result = altSQLStatement("SELECT encounter.*, location.name AS location_name FROM encounter LEFT JOIN location ON encounter.loc = location.id");
while ($row = sqlFetchArray($result)) {
  import_patient_encounters($row);
}


//sqlStatement("UPDATE import_scripts SET last_run = NOW() WHERE name = ?", array("adept_patient_import"));
sqlStatement("UPDATE import_scripts SET last_run = NOW() WHERE name = ?", array("adept_encounter_import"));

sqlClose();
?>

<html>
<head>
<title>Cronjob - Import patient data</title>
</head>
<body>
    <center>
    </center>
</body>
</html>
